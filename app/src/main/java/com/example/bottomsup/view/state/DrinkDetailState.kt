package com.example.bottomsup.view.state
import com.example.bottomsup.model.response.DrinkDetailsDTO

data class DrinkDetailState(
    val isLoading: Boolean = false,
    val drinkDetail: List<DrinkDetailsDTO.Drink> = emptyList()
)
