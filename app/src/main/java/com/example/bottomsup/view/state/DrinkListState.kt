package com.example.bottomsup.view.state

import com.example.bottomsup.model.response.CategoryDrinksDTO

data class DrinkListState(
val isLoading: Boolean = false,
val drinkslist: List<CategoryDrinksDTO.Drink> = emptyList()
)
