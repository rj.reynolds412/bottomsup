package com.example.bottomsup.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.bottomsup.databinding.FragmentCategoryBinding
import com.example.bottomsup.model.remote.adapter.CategoryAdapter
import com.example.bottomsup.viewModel.CategoryViewModel

class CategoryFragment : Fragment() {

    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter() }

    private var _binding : FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvCategory.adapter = categoryAdapter
        categoryViewModel.state.observe(viewLifecycleOwner){ state->
            categoryAdapter.loadCategory(state.categories)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}