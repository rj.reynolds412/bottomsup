package com.example.bottomsup.model.remote.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.bottomsup.databinding.ItemDrinksBinding
import com.example.bottomsup.model.response.CategoryDrinksDTO

class DrinksAdapter(private val drinkClicked: (drink: CategoryDrinksDTO.Drink)-> Unit)
    : RecyclerView.Adapter<DrinksAdapter.DrinksViewHolder>() {
    private var drinks = mutableListOf<CategoryDrinksDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder.getInstance(parent).apply {
            itemView.setOnClickListener { drinkClicked(drinks[adapterPosition]) }
        }
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drinks = drinks[position]
        holder.getDrink(drinks)
    }

    override fun getItemCount(): Int = drinks.size

    fun loadDrinks(drinks: List<CategoryDrinksDTO.Drink>){
        this.drinks.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(drinks)
            notifyItemRangeInserted(0, drinks.size  )
        }
    }

    class DrinksViewHolder(
        private val binding: ItemDrinksBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun getDrink(drink: CategoryDrinksDTO.Drink) {
            binding.tvDrink.text = drink.strDrink
            binding.ivDrink.load(drink.strDrinkThumb)
        }


        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinksBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinksViewHolder(it) }
        }
    }


}

