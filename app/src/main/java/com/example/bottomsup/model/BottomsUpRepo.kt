package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {


    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getCategoryDrinks(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getCategoryDrinks(category)
    }

    suspend fun getDrinksDetails(drinkId: Int) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinksDetails(drinkId)
    }

}