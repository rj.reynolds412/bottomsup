package com.example.bottomsup.model.remote.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemCategoryBinding
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.CategoryFragmentDirections
import com.example.bottomsup.viewModel.DrinksViewModel

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var categories = mutableListOf<CategoryDTO.CategoryItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
         val categories = (categories[position])
        holder.getCategory(categories)
        holder.binding.tvCategory.setOnClickListener { catnav ->
            catnav.findNavController().navigate(
                CategoryFragmentDirections.
                actionCategoryFragmentToDrinksFragment(categories.strCategory))
        }
    }

    override fun getItemCount(): Int = categories.size

    fun loadCategory(categories: List<CategoryDTO.CategoryItem>) {
        this.categories.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(categories)
            notifyItemRangeInserted(0, categories.size)
        }

    }

    class CategoryViewHolder(
        val binding: ItemCategoryBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun getCategory(category: CategoryDTO.CategoryItem) {
            binding.tvCategory.text = category.strCategory
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }

}