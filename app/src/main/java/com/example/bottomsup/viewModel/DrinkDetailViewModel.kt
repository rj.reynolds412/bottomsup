package com.example.bottomsup.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.state.DrinkDetailState
import kotlinx.coroutines.launch

class DrinkDetailViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _detailState = MutableLiveData(DrinkDetailState(isLoading = true))
    val detailState: LiveData<DrinkDetailState> get() = _detailState


    fun getDrinkDetails(drinkId: Int) {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinksDetails(drinkId)
            Log.e("DrinkDetailViewModel", "getDrinkDetails: ${drinkDetailsDTO.drinks}")
            _detailState.value = DrinkDetailState(drinkDetail = drinkDetailsDTO.drinks)
        }
    }
}